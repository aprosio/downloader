package eu.fbk.umcdownloader.normattiva;

import com.jaunt.*;
import eu.fbk.utils.core.CommandLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by alessio on 18/01/17.
 */

public class ScraperGU {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScraperGU.class);
    private static Integer DEFAULT_PAUSE = 500;

    public static void main(String[] args) {
        try {

            final CommandLine cmd = CommandLine
                    .parser()
                    .withName("./scrape-normattiva")
                    .withHeader("Scrape Normattiva website")
                    .withOption("o", "output", "Output folder", "FOLDER",
                            CommandLine.Type.DIRECTORY, true, false, true)
                    .withOption("w", "overwrite", "Overwrite if file exists")
                    .withOption("p", "pause",
                            String.format("Pause between pages in milliseconds (default %d)", DEFAULT_PAUSE),
                            "MILLISECONDS", CommandLine.Type.INTEGER,
                            true, false, false)
                    .withLogger(LoggerFactory.getLogger("eu.fbk")).parse(args);

            File outputFolder = cmd.getOptionValue("o", File.class);
            Integer pause = cmd.getOptionValue("p", Integer.class, DEFAULT_PAUSE);
            boolean overwrite = cmd.hasOption("w");

            outputFolder.mkdirs();

            UserAgent userAgent = new UserAgent();
            // Skip Unione Europea

            Document startPage = userAgent.visit("http://www.gazzettaufficiale.it/archivioCompleto");
            Elements types = startPage.findEach("<div id=yearGazzettaSG1>");
            for (Element type : types) {
                Element intestazione = type.findFirst("<div class=riga_t>");
                String title = intestazione.findFirst("<div>").getText().trim().replaceAll("[^0-9a-zA-Z]", "");
                LOGGER.info(title);
                if (title.equals("UnioneEuropea")) {
                    LOGGER.info("Skipping Unione Europea");
                    continue;
                }

                File typeFolder = new File(outputFolder.getAbsolutePath() + File.separator + title);
                typeFolder.mkdirs();

                Elements yearLinks = type.findEach("<a>");
                for (Element yearLink : yearLinks) {
                    String href = yearLink.getAt("href").trim();
                    String year = yearLink.findFirst("<span>").getText().trim().replaceAll("[^0-9]", "");

                    File yearFolder = new File(typeFolder.getAbsolutePath() + File.separator + year);
                    yearFolder.mkdirs();

                    LOGGER.info(title + " - " + year);

                    Document yearPage = userAgent.visit(href);
                    Element list = yearPage.findFirst("<div class=anno>");
                    Elements listLinks = list.findEach("<a>");
                    for (Element listLink : listLinks) {
                        Document dayPage;
                        try {
                            dayPage = userAgent.visit(listLink.getAt("href"));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage());
                            continue;
                        }
                        String number = listLink.innerText(null, true, true).trim().replaceAll("[^a-zA-Z0-9-]", "");

                        File numberFolder = new File(yearFolder.getAbsolutePath() + File.separator + number);
                        numberFolder.mkdirs();

                        Set<String> titles = new HashSet<>();

                        Element elenco = dayPage.findFirst("<div id=elenco_hp");
                        Elements spans = elenco.findEach("<span>");
                        String office = "";
                        for (Element span : spans) {
                            String spanClass = span.getAt("class");
                            if (spanClass.equals("emettitore")) {
                                office = span.getText().trim().replaceAll("[^A-Za-z0-9]", "");
                            }
                            if (spanClass.equals("rubrica")) {
                                office = "";
                            }
                            if (spanClass.equals("risultato")) {
                                Element docLink = span.findFirst("<a>");
                                String link = docLink.getAt("href");
                                String linkText = docLink.innerText().replaceAll("[^A-Za-z0-9]", "");

                                if (office.length() > 0) {
                                    linkText = office + "-" + linkText;
                                }

                                int i = 0;
                                while (titles.contains(linkText) && i++ < 10) {
                                    linkText += "-";
                                }

                                try {
                                    File outputFile = new File(
                                            numberFolder.getAbsolutePath() + File.separator + linkText);
                                    if (!outputFile.exists() || outputFile.length() == 0 || overwrite) {
                                        dowloadPage(link, outputFile, userAgent, pause);
                                    }
                                } catch (Exception e) {
                                    LOGGER.error("Unable to download page: " + e.getMessage());
                                }
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            CommandLine.fail(e);
        }
    }

    private static void dowloadPage(String link, File outputFile, UserAgent userAgent, Integer pause)
            throws ResponseException, SearchException, IOException, InterruptedException {
        Document document = userAgent.visit(link);

//        File outputFile = new File(numberFolder.getAbsolutePath() + File.separator + linkText);
        LOGGER.info("Writing file " + outputFile.getAbsolutePath());
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

        Elements buttons = document.findEach("<a class=stampabile>");
        String visitOther = null;
        for (Element button : buttons) {
            String buttonText = button.innerText(null, true, true).trim().replaceAll("[^a-zA-Z]", "");
            if (buttonText.equals("AttoCompleto")) {
                visitOther = button.getAt("href");
            }
        }

        if (visitOther != null) {
            document = userAgent.visit(visitOther);
            Elements exportElements = document.findEach("<div id=corpo_export>");
            if (exportElements.size() > 0) {
                document = document.submit();
            }
        }

        Element stampami = document.findFirst("<div class=wrapper_pre>");
        for (Element pre : stampami.findEach("<pre>")) {
            writer.append(pre.innerText());
            writer.append("\n");
        }

        writer.close();
        Thread.sleep(pause);
    }

//    private static void downloadResults(Document document, UserAgent userAgent, File folder, Integer pause)
//            throws ResponseException, SearchException, InterruptedException, IOException {
//        Elements each = document.findEach("<div class=risultato>");
//        if (each.size() == 0) {
//            downloadLegge(document, userAgent, folder, pause);
//            return;
//        }
//
//        for (Element result : each) {
//            Element first = result.findFirst("<a>");
//            String link = first.getAt("href");
//            Document resultDoc = userAgent.visit(link);
//            downloadLegge(resultDoc, userAgent, folder, pause);
//        }
//
//    }
//
//    private static void downloadLegge(Document document, UserAgent userAgent, File folder, Integer pause)
//            throws ResponseException,
//            SearchException, InterruptedException, IOException {
//        String printLink = document.findFirst("<div class=mini_menu>").findFirst("<a>").getAt("href");
//
//        Document printDocument = userAgent.visit(printLink);
//        Document printVersionDocument = printDocument.submit();
//        Element divPreview = printVersionDocument.findFirst("<div id=testa_atto_preview>");
//        Element pGrassetto = divPreview.findFirst("<p class=grassetto>");
//        String title = pGrassetto.innerHTML().trim().replaceAll("[^\\w]+", "-");
//
//        File outputFile = new File(folder.getAbsolutePath() + File.separator + title);
//        LOGGER.info("Writing file " + outputFile.getAbsolutePath());
//        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
//
//        Element divDescription = divPreview.findFirst("<div>");
//        String description = divDescription.innerText();
//        writer.append(description);
//        writer.append("\n");
//
//        Element stampami = printVersionDocument.findFirst("<div class=wrapper_pre>");
//        for (Element pre : stampami.findEach("<pre>")) {
//            writer.append(pre.innerText());
//            writer.append("\n");
//        }
//
//        writer.close();
//        Thread.sleep(pause);
//    }
}
