package eu.fbk.umcdownloader.normattiva;

import com.jaunt.*;
import eu.fbk.utils.core.CommandLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

/**
 * Created by alessio on 18/01/17.
 */

public class Scraper {

    private static final Logger LOGGER = LoggerFactory.getLogger(Scraper.class);
    private static Integer DEFAULT_CURRENT_YEAR = Calendar.getInstance().get(Calendar.YEAR);
    private static Integer DEFAULT_ENDING_YEAR = 1932;
    private static Integer DEFAULT_PAUSE = 500;

    public static void main(String[] args) {
        try {

            final CommandLine cmd = CommandLine
                    .parser()
                    .withName("./scrape-normattiva")
                    .withHeader("Scrape Normattiva website")
                    .withOption("o", "output", "Output folder", "FOLDER",
                            CommandLine.Type.DIRECTORY, true, false, true)
                    .withOption("s", "starting-year", "Starting year (default current year)", "YEAR",
                            CommandLine.Type.INTEGER, true, false, false)
                    .withOption("e", "ending-year", "Ending year (default 1932)", "YEAR", CommandLine.Type.INTEGER,
                            true, false, false)
                    .withOption("p", "pause",
                            String.format("Pause between pages in milliseconds (default %d)", DEFAULT_PAUSE),
                            "MILLISECONDS", CommandLine.Type.INTEGER,
                            true, false, false)
                    .withLogger(LoggerFactory.getLogger("eu.fbk")).parse(args);

            File outputFolder = cmd.getOptionValue("o", File.class);
            Integer pause = cmd.getOptionValue("p", Integer.class, DEFAULT_PAUSE);
            Integer startingYear = cmd.getOptionValue("s", Integer.class, DEFAULT_CURRENT_YEAR);
            Integer endingYear = cmd.getOptionValue("e", Integer.class, DEFAULT_ENDING_YEAR);
            if (endingYear < DEFAULT_ENDING_YEAR && endingYear > DEFAULT_CURRENT_YEAR) {
                endingYear = DEFAULT_ENDING_YEAR;
            }
            if (startingYear < DEFAULT_ENDING_YEAR && startingYear > DEFAULT_CURRENT_YEAR) {
                startingYear = DEFAULT_CURRENT_YEAR;
            }
            if (startingYear < endingYear) {
                startingYear = DEFAULT_CURRENT_YEAR;
                endingYear = DEFAULT_ENDING_YEAR;
            }
            UserAgent userAgent = new UserAgent();

            LOGGER.info(String.format("Scraping from %d to %d", endingYear, startingYear));

            for (int i = startingYear; i >= endingYear; i--) {
                File yearFolder = new File(outputFolder.getAbsolutePath() + File.separator + i);
                yearFolder.mkdirs();

                Document home = userAgent.visit("http://www.normattiva.it/ricerca/avanzata/anno/0/" + i);

                LOGGER.info("" + i + " - Fist page");
                downloadResults(home, userAgent, yearFolder, pause);

                try {
                    Element paginatore = home.findFirst("<span class=paginatore>");
                    Elements pages = paginatore.findEach("<a>");
                    for (Element page : pages) {
                        String link = page.getAt("href");
                        System.out.println("" + i + " - Page: " + link);
                        Document nextDocument = userAgent.visit(link);
                        downloadResults(nextDocument, userAgent, yearFolder, pause);
                    }
                } catch (NotFound e) {
                    // ignored
                }
            }
        } catch (Exception e) {
            CommandLine.fail(e);
        }
    }

    private static void downloadResults(Document document, UserAgent userAgent, File folder, Integer pause)
            throws ResponseException, SearchException, InterruptedException, IOException {
        Elements each = document.findEach("<div class=risultato>");
        if (each.size() == 0) {
            downloadLegge(document, userAgent, folder, pause);
            return;
        }

        for (Element result : each) {
            Element first = result.findFirst("<a>");
            String link = first.getAt("href");
            Document resultDoc = userAgent.visit(link);
            downloadLegge(resultDoc, userAgent, folder, pause);
        }

    }

    private static void downloadLegge(Document document, UserAgent userAgent, File folder, Integer pause)
            throws ResponseException,
            SearchException, InterruptedException, IOException {
        String printLink = document.findFirst("<div class=mini_menu>").findFirst("<a>").getAt("href");

        Document printDocument = userAgent.visit(printLink);
        Document printVersionDocument = printDocument.submit();
        Element divPreview = printVersionDocument.findFirst("<div id=testa_atto_preview>");
        Element pGrassetto = divPreview.findFirst("<p class=grassetto>");
        String title = pGrassetto.innerHTML().trim().replaceAll("[^\\w]+", "-");

        File outputFile = new File(folder.getAbsolutePath() + File.separator + title);
        LOGGER.info("Writing file " + outputFile.getAbsolutePath());
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

        Element divDescription = divPreview.findFirst("<div>");
        String description = divDescription.innerText();
        writer.append(description);
        writer.append("\n");

        Element stampami = printVersionDocument.findFirst("<div class=wrapper_pre>");
        for (Element pre : stampami.findEach("<pre>")) {
            writer.append(pre.innerText());
            writer.append("\n");
        }

        writer.close();
        Thread.sleep(pause);
    }
}
