package eu.fbk.umcdownloader.umc;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import eu.fbk.umcdownloader.util.SinglePageParser;
import eu.fbk.umcdownloader.util.URLpage;
import jodd.jerry.Jerry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by alessio on 10/03/16.
 */

public class Downloader {

    private static final Logger LOGGER = LoggerFactory.getLogger(Downloader.class);

    public static void main(String[] args) {

        ArrayList<String> patterns = new ArrayList<>();
        patterns.add("#lc_posts .livecomm .post | iframe | .article_img_copyright_body");

        File outputFolder = new File("/Users/alessio/Documents/out-football");
        if (!outputFolder.mkdirs()) {
            LOGGER.error("Unable to create folder {}", outputFolder.getAbsolutePath());
        }

        // Remove DEBUG log from jodd
//        ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("jodd")).setLevel(Level.ERROR);

        try {

            int i = 1;
            while (true) {

                String startingURL = "http://www.sportsmole.co.uk/football/live-commentary/";
                if (i > 1) {
                    startingURL += String.format("page-%d/", i);
                }

                LOGGER.info("Downloading {}", startingURL);

                URLpage p = new URLpage(startingURL);
                String content = p.getContent();

                if (content == null) {
                    break;
                }

                File pageFolder = new File(outputFolder.getAbsolutePath() + File.separator + i);
                if (!pageFolder.mkdirs()) {
                    LOGGER.error("Unable to create folder {}", pageFolder.getAbsolutePath());
                }

                Jerry document = Jerry.jerry(content);
                Jerry jerry = document.$("a.list_rep");
                for (Jerry j : jerry) {
                    String link = j.attr("href");
                    URLpage thisPage = new URLpage(startingURL, link);
                    URL url = thisPage.getMyURL();
                    LOGGER.info("Parsing {}", url);

                    File thisFile = new File(pageFolder.getAbsolutePath() + File.separator + url.toString()
                            .replaceAll("[^a-zA-Z0-9-]", ""));

                    if (thisFile.exists()) {
                        LOGGER.info("File {} exists, skipping", thisFile.getAbsolutePath());
                        continue;
                    }

                    String article = SinglePageParser.parse(url.toString(), patterns);
                    if (article == null) {
                        LOGGER.warn("Article is null, skipping");
                        continue;
                    }

                    Files.write(article, thisFile, Charsets.UTF_8);
                    Thread.sleep(1000);
                }

                Thread.sleep(2000);
                i++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
