package eu.fbk.umcdownloader.cat;

import com.google.common.collect.HashMultimap;
import eu.fbk.utils.core.XMLHelper;
import eu.fbk.utils.core.core.HashIndexSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.*;

/**
 * Created by alessio on 06/12/16.
 */

public class MeantimeParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(MeantimeParser.class);

    static public class Token {

        private int start, length;

        public Token(int start, int length) {
            this.start = start;
            this.length = length;
        }

        public int getStart() {
            return start;
        }

        public void setStart(int start) {
            this.start = start;
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }

        @Override public String toString() {
            return "Token{" +
                    "start=" + start +
                    ", length=" + length +
                    '}';
        }
    }

    public static void main(String[] args) {
        String folderName = "/Users/alessio/Desktop/meantime_newsreader_english_oct15/intra-doc_annotation";
        String outAnnotations = "/Users/alessio/Desktop/meantime_newsreader_english_oct15/outAnnotations.tsv";
        String outSentences = "/Users/alessio/Desktop/meantime_newsreader_english_oct15/outSentences.tsv";

        try {

            BufferedWriter annotationsWriter = new BufferedWriter(new FileWriter(outAnnotations));
            BufferedWriter sentencesWriter = new BufferedWriter(new FileWriter(outSentences));

            File baseFolder = new File(folderName);
            for (File folder : baseFolder.listFiles()) {
                if (!folder.isDirectory()) {
                    continue;
                }

                for (File file : folder.listFiles()) {
                    System.out.println(file.getAbsolutePath());

                    Document document = XMLHelper.getDocument(file);
                    NodeList nodeList;

                    // Document ID

                    nodeList = XMLHelper.getNodeList(document, "/Document");
                    String documentID = ((Element) nodeList.item(0)).getAttribute("doc_id");

                    // Tokens

                    nodeList = XMLHelper.getNodeList(document, "/Document/token");

                    StringBuffer buffer = new StringBuffer();
                    String lastSentence = "0";

                    Map<String, Token> onlyTokens = new LinkedHashMap<>();
                    Map<String, String> token2sentence = new HashMap<>();
                    Map<String, String> sentences = new LinkedHashMap<>();

                    for (int i = 0; i < nodeList.getLength(); i++) {
                        Element node = (Element) nodeList.item(i);
                        String tokenID = node.getAttribute("t_id");
                        String sentenceID = node.getAttribute("sentence");
                        if (!sentenceID.equals(lastSentence)) {
                            sentences.put(lastSentence, buffer.toString().trim());
                            buffer = new StringBuffer();
                        }
                        lastSentence = sentenceID;
                        String tokenText = node.getTextContent();
                        int offset = buffer.length();
                        buffer.append(tokenText);
                        int length = buffer.length() - offset;
                        Token token = new Token(offset, length);
                        token2sentence.put(tokenID, sentenceID);
                        onlyTokens.put(tokenID, token);
                        buffer.append(" ");
                    }

                    for (String sentence : sentences.keySet()) {
                        sentencesWriter.append(documentID).append("_").append(sentence).append("\t");
                        sentencesWriter.append(sentences.get(sentence)).append("\n");
                    }

                    // Mentions

                    Map<String, Token> mentions = new HashMap<>();
                    Map<String, String> mention2sentence = new HashMap<>();

                    nodeList = XMLHelper.getNodeList(document, "/Document/Markables/ENTITY_MENTION");
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        Element node = (Element) nodeList.item(i);
                        String syntactic_type = node.getAttribute("syntactic_type");
                        String m_id = node.getAttribute("m_id");
                        if (!syntactic_type.equals("NAM")) {
                            continue;
                        }

                        Integer start = null, end = null;
                        String sentence = null;
                        NodeList childNodes = node.getChildNodes();
                        for (int j = 0; j < childNodes.getLength(); j++) {
                            Element tokenAnchor = null;
                            try {
                                tokenAnchor = (Element) childNodes.item(j);
                            } catch (Exception e) {
                                continue;
                            }
                            String t_id = tokenAnchor.getAttribute("t_id");
                            Token token = onlyTokens.get(t_id);
                            if (sentence == null) {
                                sentence = token2sentence.get(t_id);
                            }
                            int tmpStart = token.getStart();
                            if (start == null || start > tmpStart) {
                                start = tmpStart;
                            }
                            int tmpEnd = token.getStart() + token.getLength();
                            if (end == null || end < tmpEnd) {
                                end = tmpEnd;
                            }
                        }
                        mentions.put(m_id, new Token(start, end - start));
                        mention2sentence.put(m_id, sentence);
                    }

                    // Relations

                    HashMultimap<String, String> relations = HashMultimap.create();
                    nodeList = XMLHelper.getNodeList(document, "/Document/Relations/REFERS_TO");
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        Element node = (Element) nodeList.item(i);
                        NodeList childNodes = node.getChildNodes();

                        Set<String> targets = new HashIndexSet<>();
                        Set<String> sources = new HashIndexSet<>();
                        for (int j = 0; j < childNodes.getLength(); j++) {
                            Element tokenAnchor = null;
                            try {
                                tokenAnchor = (Element) childNodes.item(j);
                            } catch (Exception e) {
                                continue;
                            }
                            String m_id = tokenAnchor.getAttribute("m_id");

                            if (tokenAnchor.getTagName().toLowerCase().equals("target")) {
                                targets.add(m_id);
                            }
                            if (tokenAnchor.getTagName().toLowerCase().equals("source")) {
                                sources.add(m_id);
                            }

                            for (String source : sources) {
                                for (String target : targets) {
                                    relations.put(target, source);
                                }

                            }

                        }

                    }

                    // Entities

                    nodeList = XMLHelper.getNodeList(document, "/Document/Markables/ENTITY");
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        Element node = (Element) nodeList.item(i);
                        String ent_type = node.getAttribute("ent_type");

//                        if (ent_type.equals("LOC") || ent_type.equals("FIN")) {
//                            continue;
//                        }

                        String instance_id = node.getAttribute("instance_id");
                        String external_ref = node.getAttribute("external_ref");
                        if (instance_id == null || instance_id.length() == 0) {
                            continue;
                        }

                        String corefID = "NIL_" + instance_id;
                        if (external_ref != null && external_ref.length() > 0) {
                            corefID = external_ref;
                        }
                        String m_id = node.getAttribute("m_id");
                        Set<String> loopSet = new HashSet<>();
                        if (relations.get(m_id) != null) {
                            loopSet.addAll(relations.get(m_id));
                        }
                        else {
                            loopSet.add(m_id);
                        }

                        for (String this_m_id : loopSet) {
                            String sentenceID = mention2sentence.get(this_m_id);
                            Token mention = mentions.get(this_m_id);
                            if (mention == null) {
                                continue;
                            }
                            int start = mention.getStart();
                            int end = start + mention.getLength();

                            StringBuffer rowBuffer = new StringBuffer();
                            rowBuffer.append(documentID).append("_").append(sentenceID).append("\t");
                            rowBuffer.append(start).append("\t").append(end).append("\t");
                            rowBuffer.append(corefID).append("\t");
                            rowBuffer.append("1").append("\t");
                            rowBuffer.append(ent_type).append("\n");

                            annotationsWriter.append(rowBuffer.toString());
//                            System.out.println(node.getAttribute("TAG_DESCRIPTOR"));
//                            System.out.println(rowBuffer.toString());
//                            System.out.println();
                        }

                    }

//                    break;
                }

//                break;
            }

            annotationsWriter.close();
            sentencesWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
