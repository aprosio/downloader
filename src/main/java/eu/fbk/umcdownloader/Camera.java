package eu.fbk.umcdownloader;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import eu.fbk.utils.core.CommandLine;
import jodd.jerry.Jerry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Created by alessio on 03/01/17.
 */

public class Camera {

    private static final Logger LOGGER = LoggerFactory.getLogger(Camera.class);
    private static String regex = "(?s)<!--.*-->";
    private static String pagRegex = "^Pag\\.\\s+[0-9]+$";

    public static void main(String[] args) {
//        File rootDir = new File("/Volumes/addenda/banchedati");
//        File outputFile = new File("/Volumes/addenda/banchedati.out");
//        boolean removeBr = false;

        try {

            final CommandLine cmd = CommandLine
                    .parser()
                    .withName("./extract-camera")
                    .withHeader("Extract text from Italian Parliament files")
                    .withOption("i", "input", "Input folder", "DIR",
                            CommandLine.Type.DIRECTORY_EXISTING, true, false, true)
                    .withOption("o", "output", "Output file", "FILE",
                            CommandLine.Type.FILE_EXISTING, true, false, true)
                    .withOption("r", "remove-br", "Remove newlines")
                    .withLogger(LoggerFactory.getLogger("eu.fbk")).parse(args);

            File rootDir = cmd.getOptionValue("input", File.class);
            File outputFile = cmd.getOptionValue("output", File.class);
            boolean removeBr = cmd.hasOption("remove-br");

            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

            for (File f : Files.fileTreeTraverser().preOrderTraversal(rootDir)) {
                if (f.isFile()) {
                    if (f.getName().equals("header.htm")) {
                        continue;
                    }
                    if (f.getName().equals("index.htm")) {
                        continue;
                    }
                    if (f.getName().equals("index.html")) {
                        continue;
                    }
                    if (f.getName().startsWith(".")) {
                        continue;
                    }

                    System.out.println(f.getAbsolutePath());

                    Jerry subDoc = null;
                    String text = null;
                    try {
                        subDoc = Jerry.jerry(Files.toString(f, Charsets.UTF_8));
                        text = subDoc.text();
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage());
                        continue;
                    }

                    text = text.replaceAll(regex, "");

                    if (removeBr) {
                        StringBuffer buffer = new StringBuffer();
                        String[] lines = text.split(System.getProperty("line.separator"));
                        boolean justAdded = false;
                        for (String line : lines) {
                            line = line.trim();
                            if (line.length() == 0) {
                                if (!justAdded) {
                                    justAdded = true;
                                    buffer.append("\n");
                                }
                                continue;
                            }
                            buffer.append(line);
                            justAdded = false;
                        }
                        text = buffer.toString();
                    } else {
                        boolean first = true;
                        char next = '\n';
                        StringBuffer buffer = new StringBuffer();
                        String[] lines = text.split(System.getProperty("line.separator"));
                        for (String line : lines) {
                            boolean skip = false;
                            line = line.trim();
                            if (line.length() == 0) {
                                continue;
                            }

                            if (line.matches(pagRegex)) {
                                skip = true;
                                next = ' ';
                            }

                            if (!first) {
                                buffer.append(next);
                            } else {
                                first = false;
                            }

                            if (skip) {
                                continue;
                            }

                            buffer.append(line);
                            next = '\n';
                        }
                        text = buffer.toString();
                    }

                    writer.append(text).append("\n\n");
//                    System.out.println(text);
//                    break;
                }
            }

            writer.close();

        } catch (Exception e) {
            CommandLine.fail(e);
        }
    }
}
