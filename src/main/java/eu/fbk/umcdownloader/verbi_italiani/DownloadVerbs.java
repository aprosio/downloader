package eu.fbk.umcdownloader.verbi_italiani;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import eu.fbk.umcdownloader.util.SinglePageParser;
import eu.fbk.utils.core.CommandLine;
import jodd.jerry.Jerry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.List;

/**
 * Created by alessio on 21/06/16.
 */

public class DownloadVerbs {

    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadVerbs.class);
    //    private static String WR_PREFIX = "http://www.wordreference.com/conj/ITverbs.aspx?v=";
    private static String WR_PREFIX = "http://www.italian-verbs.com/verbi-italiani/coniugazione.php?verbo=";

    public static void main(String[] args) {
        try {
            final CommandLine cmd = CommandLine
                    .parser()
                    .withName("wr-downloader")
                    .withHeader("Download conjugations of Italian verbs from Verbi Italiani")
                    .withOption("i", "input-path", "list of verbs", "DIR", CommandLine.Type.DIRECTORY_EXISTING, true,
                            false, true)
                    .withOption("o", "output-path", "output folder", "DIR", CommandLine.Type.DIRECTORY, true, false,
                            true)
                    .withOption("n", "not-found-path", "file for not found verbs", "DIR", CommandLine.Type.FILE, true,
                            false,
                            true)
                    .withLogger(LoggerFactory.getLogger("eu.fbk")).parse(args);

            final File inputPath = cmd.getOptionValue("i", File.class);
            final File outputPath = cmd.getOptionValue("o", File.class);
            final File notfoundPath = cmd.getOptionValue("n", File.class);

            if (outputPath.isFile()) {
                LOGGER.error("{} is a file, aborting", outputPath.getAbsolutePath());
                System.exit(1);
            }

            if (!outputPath.exists()) {
                outputPath.mkdirs();
            }

            HashSet<String> notFound = new HashSet<>();
            if (notfoundPath.exists()) {
                List<String> list = Files.readLines(notfoundPath, Charsets.UTF_8);
                for (String notFoundSingle : list) {
                    notFoundSingle = notFoundSingle.trim();
                    if (notFoundSingle.length() > 0) {
                        notFound.add(notFoundSingle);
                    }
                }
                notfoundPath.delete();
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(notfoundPath));
            for (String s : notFound) {
                writer.append(s).append("\n");
            }
            writer.flush();

            List<String> lines = Files.readLines(inputPath, Charsets.UTF_8);
            for (String line : lines) {
                line = line.trim();
                if (line.length() == 0) {
                    continue;
                }

                if (notFound.contains(line)) {
                    continue;
                }

                String url = WR_PREFIX + line;
                File outputFolder = new File(outputPath.getAbsolutePath() + File.separator + line.substring(0, 1));
                outputFolder.mkdirs();
                String outputFile = outputFolder.getAbsolutePath() + File.separator + line;
                File file = new File(outputFile);

                if (file.exists()) {
                    LOGGER.info("Already downloaded: " + line);
                    continue;
                }

                String content = SinglePageParser.parseContent(url);
                Jerry document = Jerry.jerry(content);
                int num = document.$(".verbo").length();

                Thread.sleep(100);
                if (num == 0) {
                    LOGGER.info("Not found: " + line);
                    writer.append(line).append("\n");
                    writer.flush();
                    continue;
                }

                LOGGER.info("Downladed: " + line);

                Files.write(content, file, Charsets.UTF_8);
            }

            writer.close();

        } catch (Exception e) {
            CommandLine.fail(e);
        }
    }
}
