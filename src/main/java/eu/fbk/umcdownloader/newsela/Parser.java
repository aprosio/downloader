package eu.fbk.umcdownloader.newsela;

import eu.fbk.utils.core.CommandLine;
import eu.fbk.utils.core.FrequencyHashSet;
import eu.fbk.utils.core.diff_match_patch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;

/**
 * Created by alessio on 04/01/17.
 */

public class Parser {

    private static final Logger LOGGER = LoggerFactory.getLogger(Parser.class);

    public static void main(String[] args) {
        try {
            final CommandLine cmd = CommandLine
                    .parser()
                    .withName("./extract-camera")
                    .withHeader("Extract text from Italian Parliament files")
                    .withOption("i", "input", "Input file", "FILE",
                            CommandLine.Type.FILE_EXISTING, true, false, true)
                    .withLogger(LoggerFactory.getLogger("eu.fbk")).parse(args);

            File inputFile = cmd.getOptionValue("input", File.class);

            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            String line;
            FrequencyHashSet<String> frequencies = new FrequencyHashSet<>();
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("\t");
                if (parts.length < 5) {
                    continue;
                }

                Integer diff1 = Integer.parseInt(parts[1].substring(1, 2));
                Integer diff2 = Integer.parseInt(parts[2].substring(1, 2));
                if (diff1 - diff2 != -1 && diff1 - diff2 != 1) {
                    continue;
                }

                String text1 = parts[3];
                String text2 = parts[4];

                diff_match_patch diffMatchPatch = new diff_match_patch();
                LinkedList<diff_match_patch.Diff> diffs = diffMatchPatch.diff_main(text1, text2);
                diffMatchPatch.diff_cleanupSemantic(diffs);
//                diffMatchPatch.diff_cleanupMerge(diffs);

                StringBuffer buffer = new StringBuffer();
                for (diff_match_patch.Diff diff : diffs) {
                    buffer.append(diff.operation.toString().substring(0, 1));
                }
                frequencies.add(buffer.toString());

                StringBuffer deleteBuffer = new StringBuffer();

                int delete = 0;
                int insert = 0;
                for (diff_match_patch.Diff diff : diffs) {
                    if (diff.operation == diff_match_patch.Operation.DELETE) {
                        delete++;
                        deleteBuffer.append(diff.text.trim());
                    }
                    if (diff.operation == diff_match_patch.Operation.INSERT) {
                        insert++;
                    }
                }

                System.out.println(buffer.toString());
                System.out.println(line);
                System.out.println(diffs);
                System.out.println();

                if (deleteBuffer.toString().length() == 0) {
                    continue;
                }

                if (insert == 0 && delete == 1 && diffs.size() == 3) {
                    boolean spaceBefore = (
                            !Character.isAlphabetic(diffs.get(0).text.charAt(diffs.get(0).text.length() - 1)))
                            ||
                            !Character.isAlphabetic(diffs.get(1).text.charAt(0));
                    boolean spaceAfter = (
                            !Character.isAlphabetic(diffs.get(1).text.charAt(diffs.get(1).text.length() - 1)))
                            ||
                            !Character.isAlphabetic(diffs.get(2).text.charAt(0));

                    if (spaceAfter && spaceBefore) {
//                        System.out.println(diffs);
                    }
                }
            }
            reader.close();

            System.out.println(frequencies.getSorted());

        } catch (Exception e) {
            CommandLine.fail(e);
        }
    }
}
