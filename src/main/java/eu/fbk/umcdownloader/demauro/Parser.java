package eu.fbk.umcdownloader.demauro;

import eu.fbk.utils.core.IO;
import jodd.jerry.Jerry;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by alessio on 14/06/16.
 */

public class Parser {

    private static final Logger LOGGER = LoggerFactory.getLogger(Parser.class);

    static {
        HashSet<String> skipFiles = new HashSet<>();
    }

    public static void main(String[] args) {

        String inputDir = "/Users/alessio/Documents/out-demauro";
        String outputVerbs = "/Users/alessio/Documents/out-demauro/verbs.txt";
        Integer limit = null;

        // ---

        Iterator<File> fileIterator = FileUtils.iterateFiles(new File(inputDir), null, true);

        HashSet<String> qualifiche = new HashSet<>();
        HashSet<String> verbi = new HashSet<>();

        AtomicInteger i = new AtomicInteger();
        fileIterator.forEachRemaining((File f) -> {

            String fileName = f.getAbsolutePath();
            if (!f.isFile() || f.getName().startsWith(".")) {
                return;
            }
            if (limit != null) {
                if (i.incrementAndGet() > limit) {
                    return;
                }
            }
            try {
                InputStream read = IO.read(f.getAbsolutePath());
                StringWriter writer = new StringWriter();
                IOUtils.copy(read, writer, Charset.defaultCharset());
                String theString = writer.toString();

                Jerry document = Jerry.jerry(theString);
                document.$("#lemma h1 sup").remove();
                String title = document.$("#lemma h1").first().text().trim();

//                System.out.println(fileName);
//                System.out.println(title.first().text().trim());

                Jerry qualifica = document.$("#lemma span.qualifica");
                for (Jerry q1 : qualifica) {
//                    System.out.println(q1.text().trim());
                    String qTrimmed = q1.text().trim();
                    qTrimmed = qTrimmed.replaceAll("\\([^\\)]+\\)", "").trim();
                    if (qTrimmed.startsWith("v.")) {
                        verbi.add(title);
                    }
                    String[] qs = qTrimmed.split(",");
                    for (String q : qs) {
                        q = q.trim();
                        qualifiche.add(q);
                    }
                }

//                System.out.println();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputVerbs));
            for (String verb : verbi) {
                writer.append(verb).append("\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(qualifiche);
        System.out.println(verbi);
//        IO.read()
    }
}
