package eu.fbk.umcdownloader.sabatinicoletti;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import eu.fbk.dh.tint.readability.DescriptionForm;
import eu.fbk.dh.tint.readability.GlossarioEntry;
import eu.fbk.dh.tint.readability.it.ItalianReadability;
import eu.fbk.dh.tint.runner.TintPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * Created by alessio on 18/12/16.
 */

public class Annotator {

    private static final Logger LOGGER = LoggerFactory.getLogger(Annotator.class);

    public static void main(String[] args) {
        File inputFile = new File("/Users/alessio/Documents/out-sinonimicontrari-lemmatized-noinvert.txt");
        HashMap<String, GlossarioEntry> glossario = new HashMap<>();
        HashMap<String, LinkedTreeMap> glossarioTmp;
        TreeMap<Integer, DescriptionForm> forms = new TreeMap<>();
        String text = "Ai sensi del regolamento per la disciplina del sistema dei servizi socio educativi per la prima infanzia approvato con deliberazione del Consiglio comunale 28/06/2007, n.70.";

        try {
            TintPipeline pipeline = new TintPipeline();
            pipeline.loadDefaultProperties();
            pipeline.setProperty("annotators", "ita_toksent, pos, ita_morpho, ita_lemma");
            pipeline.load();

            int lemmaIndex = 0;
//            HashMap<Integer, Integer> lemmaIndexes = new HashMap<>();
            HashMap<Integer, Integer> tokenIndexes = new HashMap<>();
            HashMap<Integer, Integer> endIndexes = new HashMap<>();

            Annotation annotation = pipeline.runRaw(text);
            StringBuffer lemmaText = new StringBuffer();
            for (CoreLabel token : annotation.get(CoreAnnotations.TokensAnnotation.class)) {
                lemmaText.append(token.lemma()).append(" ");
//                lemmaIndexes.put(lemmaText.length(), lemmaIndex);
                tokenIndexes.put(token.get(CoreAnnotations.CharacterOffsetBeginAnnotation.class), lemmaIndex);
                endIndexes.put(token.get(CoreAnnotations.CharacterOffsetBeginAnnotation.class),
                        token.get(CoreAnnotations.CharacterOffsetEndAnnotation.class));

                lemmaIndex++;
            }

            Gson gson = new GsonBuilder().create();
            glossarioTmp = gson.fromJson(Files.toString(inputFile, Charsets.UTF_8), HashMap.class);

            List<String> glossarioKeys = new ArrayList<>(glossarioTmp.keySet());
            Collections.sort(glossarioKeys, new ItalianReadability.StringLenComparator());

            for (String form : glossarioKeys) {
                LinkedTreeMap linkedTreeMap = glossarioTmp.get(form);

                ArrayList<String> arrayList = (ArrayList<String>) linkedTreeMap.get("forms");
                String[] strings = new String[arrayList.size()];
                strings = arrayList.toArray(strings);
//                String[] strings = (String[]) linkedTreeMap.get("forms");
                String description = (String) linkedTreeMap.get("description");
                GlossarioEntry entry = new GlossarioEntry(strings, description);
                glossario.put(form, entry);
            }

            for (String form : glossarioKeys) {

                if (form.length() < 4) {
                    continue;
                }

                int numberOfTokens = form.split("\\s+").length;
                List<Integer> allOccurrences = ItalianReadability.findAllOccurrences(text, form);
//                List<Integer> allLemmaOccurrences = ItalianReadability
//                        .findAllOccurrences(lemmaText.toString().trim(), form);
//                if (allLemmaOccurrences.size() > 0) {
//                    System.out.println(form);
//                    System.out.println(allLemmaOccurrences);
//                }

                for (Integer occurrence : allOccurrences) {
                    ItalianReadability
                            .addDescriptionForm(form, tokenIndexes, occurrence, numberOfTokens, forms, annotation,
                                    glossario);
                }
//                for (Integer occurrence : allLemmaOccurrences) {
//                    ItalianReadability
//                            .addDescriptionForm(form, lemmaIndexes, occurrence, numberOfTokens, forms, annotation,
//                                    glossario);
//                }
            }

            for (Integer integer : forms.keySet()) {
                Integer end = endIndexes.get(integer);
                if (end == null) {
                    continue;
                }
                System.out.println(integer);
                System.out.println(forms.get(integer));
            }

//            System.out.println(forms);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
