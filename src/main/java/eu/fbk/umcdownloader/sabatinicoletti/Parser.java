package eu.fbk.umcdownloader.sabatinicoletti;

import eu.fbk.utils.core.IO;
import jodd.jerry.Jerry;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;

/**
 * Created by alessio on 30/11/16.
 */

public class Parser {

    private static final Logger LOGGER = LoggerFactory.getLogger(Parser.class);

    public static void main(String[] args) {

        String outputFile = "/Users/alessio/Documents/out-sinonimicontrari.txt";

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

            File inputFolder = new File("/Users/alessio/Documents/out-sinonimicontrari");
            for (File letterFolder : inputFolder.listFiles()) {
                if (letterFolder.getName().startsWith(".")) {
                    continue;
                }

                for (File file : letterFolder.listFiles()) {
                    InputStream read = IO.read(file.getAbsolutePath());
                    StringWriter sWriter = new StringWriter();
                    IOUtils.copy(read, sWriter, Charset.defaultCharset());
                    String theString = sWriter.toString();

                    Jerry document = Jerry.jerry(theString);

                    document.$("#defin-dx h5 sup").remove();

                    Jerry list = document.$("#defin-dx > *");
                    String word = null;
                    String type = null;
                    String id = "1";
                    String synContr = "SYN";

                    for (Jerry tag : list) {
                        if (tag.is("h5")) {
                            word = tag.$("strong").first().text().trim();
                            type = tag.$("strong").last().text().trim();
                            synContr = "SYN";
                            continue;
                        }

//                        System.out.println(word);
//                        System.out.println(type);

                        for (Jerry p : tag.$("li")) {

                            for (Jerry span : p.$("span")) {
                                String spanText = span.text().trim();
                                if (spanText.toLowerCase().equals("contr")) {
                                    synContr = "CONTR";
                                }
//                                System.out.println("SPAN: " + spanText);
                            }
                            for (Jerry strong : p.$("strong")) {
                                id = strong.text().replaceAll("\\.", "").trim();
                            }

                            p.$("span, strong").remove();
                            String def = p.text().trim();
                            def = def.replaceAll("•", "").trim();
                            def = def.replaceAll("\\([^)]*\\)", "").trim();

                            if (def.endsWith(".")) {
                                def = def.substring(0, def.length() - 1).trim();
                            }
                            if (def.length() == 0) {
                                continue;
                            }

                            String[] parts = def.split("\\s*[,;:]\\s*");
//                            System.out.println(Arrays.toString(parts));
                            for (String part : parts) {
                                writer.append(word).append('\t');
                                writer.append(type).append('\t');
                                writer.append(synContr).append('\t');
                                writer.append(id).append('\t');
                                writer.append(part).append('\n');
                            }

//                            System.out.println("ID: " + id);
//                            System.out.println(def);
                        }
                    }

//                    System.out.println();
                }
            }

            writer.close();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

    }
}
