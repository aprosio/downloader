package eu.fbk.umcdownloader.sabatinicoletti;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import eu.fbk.umcdownloader.util.SinglePageParser;
import eu.fbk.umcdownloader.util.URLpage;
import jodd.jerry.Jerry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;

/**
 * Created by alessio on 10/03/16.
 */

public class DownloaderSC {

    private static final Logger LOGGER = LoggerFactory.getLogger(DownloaderSC.class);

    public static void main(String[] args) {

        File outputFolder = new File("/Users/alessio/Documents/out-sinonimicontrari");
        if (!outputFolder.mkdirs()) {
            LOGGER.error("Unable to create folder {}", outputFolder.getAbsolutePath());
        }

        // Remove DEBUG log from jodd
//        ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("jodd")).setLevel(Level.ERROR);

        try {

            for (char alphabet = 'a'; alphabet <= 'z'; alphabet++) {

                File pageFolder = new File(outputFolder.getAbsolutePath() + File.separator + alphabet);
                if (!pageFolder.mkdirs()) {
                    LOGGER.error("Unable to create folder {}", pageFolder.getAbsolutePath());
                }

                int i = 1;
                while (true) {

                    String startingURL =
                            "http://dizionari.corriere.it/dizionario_sinonimi_contrari/" + alphabet + ".shtml";
                    if (i > 1) {
                        startingURL = "http://dizionari.corriere.it/dizionario_sinonimi_contrari/" + alphabet + "_" + i
                                + ".shtml";
                    }

                    LOGGER.info("Downloading {}", startingURL);

                    URLpage p = new URLpage(startingURL);
                    String content = p.getContent();

                    if (content == null) {
                        break;
                    }

                    Jerry document = Jerry.jerry(content);
                    Jerry jerry = document.$("#ris-main a");
                    if (jerry.size() == 0) {
                        break;
                    }

                    for (Jerry j : jerry) {
                        String link = j.attr("href");
                        URLpage thisPage = new URLpage(startingURL, link);
                        URL url = thisPage.getMyURL();
//                        LOGGER.debug("Parsing {}", url);

                        File thisFile = new File(pageFolder.getAbsolutePath() + File.separator + url.toString()
                                .replaceAll("[^a-zA-Z0-9-]", ""));

                        if (thisFile.exists()) {
//                            LOGGER.info("File {} exists, skipping", thisFile.getAbsolutePath());
                            continue;
                        }

                        String article = SinglePageParser.parseContent(url.toString());
                        if (article == null) {
                            LOGGER.warn("Article is null, skipping");
                            continue;
                        }

                        Files.write(article, thisFile, Charsets.UTF_8);
                        Thread.sleep(100);
                    }

                    Thread.sleep(200);
                    i++;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
