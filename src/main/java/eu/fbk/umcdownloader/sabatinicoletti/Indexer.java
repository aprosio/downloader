package eu.fbk.umcdownloader.sabatinicoletti;

import com.google.common.base.Charsets;
import com.google.common.collect.TreeMultimap;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import eu.fbk.dh.tint.readability.GlossarioEntry;
import eu.fbk.dh.tint.runner.TintPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by alessio on 17/12/16.
 */

public class Indexer {

    private static final Logger LOGGER = LoggerFactory.getLogger(Indexer.class);
    static Pattern posPattern = Pattern.compile("^([a-zA-Z]+)\\.");
    static Map<String, String> posConversion = new HashMap<>();
    static boolean parseGlossario = true;
    static int limit = 0;
    static boolean invert = false;

    static {
        posConversion.put("agg", "A");
        posConversion.put("s", "S");
        posConversion.put("v", "V");
        posConversion.put("avv", "B");
    }

    static String getLemmas(String text, TintPipeline pipeline, Map<String, String> lemmas) {
        text = text.toLowerCase();
        if (lemmas.containsKey(text)) {
            return lemmas.get(text);
        }
        Annotation annotation = pipeline.runRaw(text);
        StringBuffer buffer = new StringBuffer();
        for (CoreLabel token : annotation.get(CoreAnnotations.TokensAnnotation.class)) {
            buffer.append(token.lemma()).append(" ");
        }

        lemmas.put(text, buffer.toString().trim());
        return lemmas.get(text);
    }

    public static void main(String[] args) {
        File file = new File("/Users/alessio/Documents/out-sinonimicontrari.txt");
        File stopWordsFile = new File("/Users/alessio/Documents/scripts/resources.twm/stopwords/it.stop");
        File outputFile = new File("/Users/alessio/Documents/out-sinonimicontrari-lemmatized-noinvert.txt");

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();

        HashMap<String, GlossarioEntry> glossario = new HashMap<>();

        try {

            TintPipeline pipeline = new TintPipeline();
            pipeline.loadDefaultProperties();
            pipeline.setProperty("annotators", "ita_toksent, pos, ita_morpho, ita_lemma");
            pipeline.load();

            List<String> lines = Files.readLines(file, Charsets.UTF_8);
            List<String> stopwordsList = Files.readLines(stopWordsFile, Charsets.UTF_8);
            HashSet<String> stopwords = new HashSet<>();

            HashMap<String, TreeMultimap<Integer, String>> words = new HashMap<>();

            for (String s : stopwordsList) {
                s = s.trim();
                if (s.length() > 0) {
                    stopwords.add(s);
                }
            }

            int i = 0;
            for (String line : lines) {
                i++;
                if (limit > 0 && limit < i) {
                    break;
                }

                String[] parts = line.split("\t");
                if (parts.length < 5) {
                    continue;
                }

                String word = parts[0];
                if (word.length() < 3) {
                    continue;
                }
                if (stopwords.contains(word)) {
                    continue;
                }

                // todo: check if word is easy

                String posString = parts[1];
                Matcher matcher = posPattern.matcher(posString);
                if (!matcher.find()) {
                    continue;
                }
                String pos = matcher.group(1);
                pos = posConversion.get(pos);
                if (pos == null) {
                    continue;
                }

                String typeString = parts[2];
                boolean isSyn = typeString.equals("SYN");
                if (!isSyn) {
                    continue;
                }

                Integer id;
                try {
                    id = Integer.parseInt(parts[3]);
                } catch (Exception e) {
                    continue;
                }

                String syn = parts[4];

                if (word.contains(" ")) {
                    continue;
                }

//                word = word + "_" + pos;

                words.putIfAbsent(word, TreeMultimap.create());
                words.get(word).put(id, syn);
            }

            for (String word : words.keySet()) {

                HashSet<String> strings = new HashSet<>();
                strings.add(word);

                StringBuffer description = new StringBuffer();

                TreeMultimap<Integer, String> mm = words.get(word);
                for (Integer key : mm.keySet()) {
                    description.append(key).append(". ");
                    NavigableSet<String> forms = mm.get(key);
                    for (String form : forms) {

                        if (invert) {
                            strings.add(form);
                        }
                        description.append(form).append(", ");

                        if (parseGlossario && invert) {
                            Annotation annotation = pipeline.runRaw(form);
                            StringBuffer stringBuffer = new StringBuffer();
                            List<CoreLabel> tokens = annotation.get(CoreAnnotations.TokensAnnotation.class);
                            for (CoreLabel token : tokens) {
                                stringBuffer.append(token.get(CoreAnnotations.LemmaAnnotation.class)).append(" ");
                            }

                            strings.add(stringBuffer.toString().trim());

//                            String pos = entry.getPos();
//                            String annotatedPos = tokens.get(0).get(CoreAnnotations.PartOfSpeechAnnotation.class);
//                            if (pos == null || annotatedPos.substring(0, 1).equals("S")) {
//                                glossario.put(stringBuffer.toString().trim(), entry);
//                            }
                        }

                    }
                    int lastComma = description.lastIndexOf(",");
                    description.delete(lastComma, lastComma + 2);
                    description.append("\n");
                }

                GlossarioEntry entry = new GlossarioEntry(word, description.toString().trim());
                for (String string : strings) {
                    glossario.put(string, entry);
                }

            }

            Writer w = new FileWriter(outputFile);
            w.write(gson.toJson(glossario));
            w.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
